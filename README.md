# A routing-packing problem

This repository contains instances and solutions for the problem
defined in *Local search techniques for a routing-packing problem* by
Sara Ceschia,  Andrea Schaerf and Thomas Stützle, Computers & Industrial Engineering, 66(4):1138-1149, 2013.

## Instances

Instances are stored in folder [`Instances`](Instances). 

## Solutions

Best solutions are available in the folder [`Solutions`](Solutions).

