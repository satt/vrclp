Total_Vehicles 
8

Vehicle vt0 nodes: 
0 10 25 4 2 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-1 0 47 16 17 11 12
25-3 0 30 18 12 15 11
25-1 0 47 0 24 13 16
25-2 0 32 0 13 15 18
4-3 14 0 10 10 12 17
4-1 14 0 0 10 34 10
4-2 7 0 0 7 32 13
2-1 0 0 0 7 30 17


Vehicle vt0 nodes: 
0 24 18 22 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
24-1 13 9 15 11 24 11
24-3 0 0 23 15 9 7
24-2 0 40 0 7 19 7
18-2 0 35 0 22 5 7
18-1 13 0 0 9 32 15
18-3 0 0 14 13 35 9
22-1 0 0 0 11 34 14


Vehicle vt0 nodes: 
0 21 20 6 23 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 13 18 0 7 34 18
20-2 0 7 15 12 28 13
20-1 0 40 0 13 16 9
6-2 13 0 19 11 18 9
6-1 13 0 8 12 18 11
23-2 0 7 0 13 33 15
23-1 13 0 0 11 17 8
23-3 0 0 0 13 7 17


Vehicle vt0 nodes: 
0 12 11 8 9 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-1 0 48 15 7 12 15
11-2 15 22 0 5 24 13
11-1 15 0 0 8 22 9
8-3 0 48 0 19 12 15
8-2 0 14 17 15 27 11
8-1 0 14 0 15 34 17
9-1 0 0 0 13 14 18


Vehicle vt0 nodes: 
0 16 13 7 1 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
16-1 11 7 15 14 36 15
13-3 0 43 0 25 12 8
13-1 11 7 0 14 36 15
13-2 0 7 8 5 34 15
7-2 0 0 9 13 7 14
7-1 0 7 0 11 36 8
1-1 0 0 0 24 7 9


Vehicle vt0 nodes: 
0 3 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
3-1 0 0 0 9 33 7


Vehicle vt0 nodes: 
0 19 15 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
19-3 13 21 0 9 33 15
19-1 0 21 0 13 30 13
19-2 0 14 0 21 7 11
15-2 0 8 0 19 6 9
15-1 0 0 0 20 8 18


Vehicle vt0 nodes: 
0 17 5 14 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-2 13 18 12 7 34 12
17-3 0 51 0 13 7 17
17-1 14 18 0 11 34 12
5-1 0 18 11 13 33 17
14-2 0 18 0 14 31 11
14-1 0 5 0 24 13 16
14-3 0 0 0 19 5 14



Rounds: 4 Idle rounds: 0
Seed: 1855454115
Hard_constraints: 0 0
Time: 263.713
Cost: 630139
