Total_Vehicles 
6

Vehicle vt0 nodes: 
0 12 6 1 2 16 15 3 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-2 0 51 12 16 6 16
12-1 15 17 13 6 34 15
6-1 13 0 18 8 15 7
1-1 15 17 0 10 34 13
2-1 15 0 7 6 17 11
16-2 0 33 12 13 17 15
16-1 15 0 0 7 13 7
15-1 0 33 0 13 26 12
3-1 0 0 0 15 33 16


Vehicle vt0 nodes: 
0 13 10 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-2 0 35 0 24 7 7
13-1 0 21 0 9 14 11
13-3 0 11 0 15 10 8
10-1 0 0 0 18 11 8


Vehicle vt0 nodes: 
0 21 4 5 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 0 35 14 24 15 8
21-2 12 35 0 13 14 14
4-2 14 0 0 5 35 15
4-1 0 0 6 14 28 17
5-1 0 35 0 12 21 14
5-2 0 0 0 14 35 6


Vehicle vt0 nodes: 
0 20 22 17 14 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
20-1 13 20 17 10 36 10
20-2 13 19 9 10 29 8
20-3 13 19 0 8 29 9
22-2 0 20 15 8 22 11
22-3 13 0 0 12 19 17
22-1 0 0 19 13 18 11
17-1 0 21 0 13 32 15
17-2 0 0 10 13 20 9
14-1 0 0 0 10 21 10


Vehicle vt0 nodes: 
0 18 19 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
18-2 0 0 18 11 32 9
18-1 9 30 0 15 30 16
18-3 0 33 0 9 14 10
19-2 0 0 11 12 30 7
19-1 0 0 0 9 33 11


Vehicle vt0 nodes: 
0 7 8 9 11 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
7-1 14 40 13 11 15 17
7-2 0 54 0 22 6 12
8-2 14 13 13 11 27 17
8-1 14 18 0 11 36 13
8-3 7 18 7 7 34 16
9-2 0 18 7 7 28 17
9-1 0 18 0 13 36 7
11-3 0 6 9 24 7 7
11-1 0 6 0 20 12 9
11-2 0 0 0 18 6 10



Rounds: 3 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 961696159
Hard_constraints: 0 0
Time: 189.524
Cost: 805430
