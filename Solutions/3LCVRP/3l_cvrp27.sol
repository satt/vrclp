Total_Vehicles 
23

Vehicle vt0 nodes: 
0 18 45 17 84 83 89 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
18-1 14 24 0 11 27 18
45-1 8 19 8 5 32 14
17-2 13 0 8 8 24 10
17-1 13 0 0 12 21 8
84-1 0 24 8 8 35 14
83-1 0 24 0 14 33 8
89-2 0 0 13 8 24 11
89-1 0 0 0 13 19 13


Vehicle vt0 nodes: 
0 42 43 15 41 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
42-3 0 33 0 7 22 16
42-2 12 0 22 11 21 8
42-1 10 29 18 11 21 12
43-2 12 0 12 10 18 10
43-1 10 30 0 14 29 18
15-1 0 0 24 12 29 6
41-2 11 0 0 14 30 12
41-3 0 0 16 11 24 8
41-1 0 0 0 10 33 16


Vehicle vt0 nodes: 
0 31 32 30 70 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
31-3 0 47 17 20 12 6
31-1 0 33 17 15 14 7
31-2 15 0 17 5 30 8
32-2 0 49 0 20 10 17
32-3 11 33 0 11 14 17
32-1 15 0 0 10 28 17
30-1 0 34 0 11 15 17
30-2 10 0 13 5 33 15
70-3 10 0 0 5 27 13
70-2 0 0 16 10 31 12
70-1 0 0 0 8 34 16


Vehicle vt0 nodes: 
0 80 24 81 51 1 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
80-1 12 20 15 13 33 15
80-3 0 20 17 12 34 11
80-2 10 23 0 14 28 15
24-1 10 0 19 12 19 10
81-1 0 34 0 9 19 17
81-2 10 0 12 13 20 7
51-1 10 0 0 14 23 12
1-1 0 0 0 10 34 17


Vehicle vt0 nodes: 
0 19 49 64 11 62 88 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
19-1 19 12 0 6 32 13
49-1 8 0 15 12 12 9
64-2 11 0 0 14 12 15
64-3 0 17 8 7 34 14
64-1 7 17 21 12 32 7
11-1 7 38 9 10 12 12
62-3 0 0 23 6 17 7
62-2 7 13 9 11 25 12
62-1 7 16 0 11 35 9
88-3 0 16 0 7 35 8
88-1 0 0 6 8 13 17
88-2 0 0 0 11 16 6


Vehicle vt0 nodes: 
0 52 82 58 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
52-1 0 45 7 19 7 14
52-2 0 45 0 17 12 7
52-3 15 0 10 7 27 16
82-1 15 0 0 10 35 10
82-2 0 0 18 15 33 12
58-1 0 13 0 15 32 18
58-2 0 0 0 12 13 18


Vehicle vt0 nodes: 
0 25 55 29 76 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
25-1 0 31 15 6 28 10
55-1 9 31 9 14 15 18
55-2 9 31 0 12 29 9
55-3 0 31 0 9 23 15
29-1 0 0 22 7 22 8
29-3 14 0 14 6 31 15
29-2 14 0 0 11 29 14
76-2 0 0 14 14 31 8
76-1 0 0 0 14 28 14


Vehicle vt0 nodes: 
0 40 73 22 74 72 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
40-3 15 30 9 7 17 18
40-2 0 26 23 8 28 6
40-1 0 26 17 11 25 6
73-3 15 30 0 10 19 9
73-2 0 12 15 17 14 14
73-1 0 29 7 13 27 10
22-1 0 29 0 15 28 7
74-2 15 0 0 9 30 8
74-3 0 0 15 15 12 10
74-1 0 0 9 14 28 6
72-1 0 0 0 14 29 9


Vehicle vt0 nodes: 
0 68 3 77 53 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
68-1 15 24 0 9 20 15
68-3 0 25 10 13 35 16
68-2 0 9 21 13 16 8
3-1 0 9 9 22 15 12
77-1 0 24 0 15 31 10
77-2 0 0 9 15 9 10
53-2 11 0 0 10 24 9
53-1 0 0 0 11 24 9


Vehicle vt0 nodes: 
0 27 10 63 90 69 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
27-1 0 31 12 8 19 9
10-1 13 23 18 9 29 7
63-1 0 31 0 12 21 12
63-2 7 0 16 15 18 6
90-3 20 23 0 5 35 12
90-2 13 23 0 7 35 18
90-1 13 0 0 12 23 16
69-2 0 0 16 7 25 14
69-3 0 0 6 11 20 10
69-1 0 0 0 13 31 6


Vehicle vt0 nodes: 
0 96 99 93 95 94 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
96-2 11 46 0 13 12 16
96-3 11 24 17 10 22 9
96-1 11 24 0 10 20 17
99-1 10 0 13 9 24 15
99-2 6 25 8 5 22 16
93-2 0 12 16 9 13 13
93-1 10 0 0 13 24 13
95-1 0 25 8 6 35 14
95-2 0 31 0 11 27 8
94-3 0 0 16 10 12 9
94-2 0 0 8 8 25 8
94-1 0 0 0 8 31 8


Vehicle vt0 nodes: 
0 91 86 16 37 92 87 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
91-1 12 36 17 12 18 6
91-2 0 22 19 12 33 10
86-3 0 54 0 22 6 10
86-2 10 36 0 11 18 17
86-1 15 0 14 10 36 16
16-1 15 0 0 10 28 14
16-2 0 22 7 10 32 12
37-1 0 0 7 13 22 15
92-1 0 34 0 10 14 7
87-1 0 0 0 15 34 7


Vehicle vt0 nodes: 
0 33 71 35 65 66 20 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
33-3 0 30 23 6 29 7
33-2 0 31 16 9 29 7
33-1 0 17 17 15 13 12
71-1 17 31 14 5 25 15
35-2 9 31 14 8 21 8
35-1 9 31 0 13 20 14
65-1 12 0 17 9 17 9
66-1 0 31 0 9 26 16
66-2 0 20 0 25 11 17
20-3 12 0 10 10 20 7
20-2 0 0 10 12 11 9
20-1 0 0 0 24 15 10


Vehicle vt0 nodes: 
0 21 4 54 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 5 33 17 15 20 9
4-2 5 33 0 13 19 17
4-1 5 13 12 14 20 15
54-3 5 0 12 10 13 17
54-1 5 0 0 15 33 12
54-2 0 0 0 5 34 18


Vehicle vt0 nodes: 
0 98 100 14 38 44 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
98-2 12 41 10 13 10 9
98-1 12 23 10 8 18 13
100-3 0 23 10 12 28 17
100-2 16 23 0 6 34 10
100-1 5 23 0 11 33 10
14-3 0 23 0 5 27 10
14-1 15 0 11 7 23 15
14-2 0 0 13 12 20 8
38-1 15 0 0 9 23 11
44-1 0 0 0 15 23 13


Vehicle vt0 nodes: 
0 60 5 61 85 59 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
60-3 0 38 15 12 15 15
60-1 10 13 12 14 21 17
60-2 0 13 15 10 25 10
5-2 10 45 0 10 15 11
5-1 10 13 0 14 32 12
61-1 9 0 0 14 13 18
85-1 0 17 0 10 35 15
59-1 0 0 0 9 17 8


Vehicle vt0 nodes: 
0 39 67 23 56 75 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
39-1 15 40 0 10 16 16
39-2 0 26 17 14 28 12
67-1 0 33 7 13 25 10
67-2 15 24 0 9 16 18
23-2 6 0 10 7 33 7
23-1 0 31 0 15 23 7
56-1 0 0 10 6 26 8
75-1 13 0 0 12 24 16
75-2 0 0 0 13 31 10


Vehicle vt0 nodes: 
0 26 12 28 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
26-2 0 33 0 9 26 13
26-1 11 0 16 14 34 9
12-3 12 13 0 13 32 16
12-1 0 0 13 11 32 8
12-2 12 0 0 13 13 16
28-1 0 0 0 12 33 13


Vehicle vt0 nodes: 
0 7 48 47 36 46 8 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
7-1 16 0 10 8 19 10
48-1 0 0 17 10 20 7
47-2 10 30 14 11 27 13
47-1 0 36 8 7 22 18
36-2 0 0 9 10 35 8
36-3 16 0 0 7 25 10
36-1 0 36 0 7 17 8
46-1 11 30 0 10 28 14
46-2 11 0 0 5 30 15
8-1 0 0 0 11 36 9


Vehicle vt0 nodes: 
0 13 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-2 0 42 0 19 11 17
13-3 0 24 0 9 18 16
13-1 0 0 0 13 24 15


Vehicle vt0 nodes: 
0 6 97 57 2 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
6-1 18 13 12 7 31 14
97-3 14 33 0 8 22 12
97-2 18 0 12 7 13 18
97-1 18 0 0 7 33 12
57-1 0 20 12 13 31 17
2-1 0 27 0 14 32 12
2-3 12 0 0 6 20 18
2-2 0 0 0 12 27 12


Vehicle vt0 nodes: 
0 79 78 34 9 50 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
79-1 13 33 14 9 24 16
79-2 13 0 14 7 33 13
78-3 0 22 22 13 18 8
78-1 13 27 0 12 26 14
78-2 13 13 0 12 14 14
34-2 0 0 22 8 22 8
34-1 13 0 0 11 13 14
9-3 0 34 15 9 15 7
9-1 0 0 15 11 34 7
9-2 0 31 0 13 24 15
50-1 0 0 0 13 31 15


Vehicle vt0 nodes: 
0 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)



Rounds: 3 Idle rounds: 1
Kick Rounds: 2 Kick improving rounds: 1
Seed: 1866291122
Hard_constraints: 0 0
Time: 5014.23
Cost: 1566732
