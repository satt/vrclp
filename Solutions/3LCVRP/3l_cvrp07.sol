Total_Vehicles 
6

Vehicle vt0 nodes: 
0 12 11 13 9 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-1 12 31 16 9 25 9
12-2 12 31 8 10 24 8
11-3 0 31 14 7 29 11
11-1 15 0 12 10 24 18
11-2 15 0 0 10 18 12
13-1 12 31 0 13 28 8
13-2 0 0 17 14 28 9
9-2 0 31 0 12 25 14
9-1 0 0 0 15 31 17


Vehicle vt0 nodes: 
0 21 4 5 8 7 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 0 38 0 8 12 12
4-3 11 34 18 14 14 7
4-1 0 19 14 8 19 13
4-2 11 34 10 12 13 8
5-2 11 19 10 13 15 7
5-3 14 0 11 8 15 11
5-1 11 19 0 14 34 10
8-3 15 0 0 9 16 11
8-2 0 19 0 11 19 14
8-1 0 0 9 14 19 16
7-1 0 0 0 15 19 9


Vehicle vt0 nodes: 
0 3 16 15 14 17 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
3-3 0 29 20 9 15 8
3-1 12 43 16 10 17 11
3-2 12 29 16 12 14 8
16-1 0 14 16 20 15 14
15-2 15 0 0 6 29 8
15-1 0 30 11 12 28 9
14-1 0 0 16 13 14 12
17-2 12 30 0 11 30 16
17-3 0 30 0 12 21 11
17-1 0 0 0 15 30 16


Vehicle vt0 nodes: 
0 20 22 19 18 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
20-2 10 29 12 11 13 12
20-1 0 54 0 23 5 18
22-1 10 9 12 15 20 14
22-2 0 9 9 10 31 12
19-3 0 41 0 15 13 6
19-1 11 9 0 13 32 12
19-2 0 9 0 11 27 9
18-1 0 0 0 18 9 10


Vehicle vt0 nodes: 
0 10 6 1 2 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-1 0 32 12 8 22 7
6-1 0 32 0 7 19 12
6-2 17 27 0 8 16 9
6-3 19 0 0 6 19 6
1-3 6 0 11 11 32 18
1-2 0 0 15 6 31 15
1-1 6 0 0 13 27 11
2-1 0 0 0 6 32 15


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 1699870609
Hard_constraints: 0 0
Time: 176.087
Cost: 752754
