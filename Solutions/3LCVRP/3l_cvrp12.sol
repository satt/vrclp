Total_Vehicles 
9

Vehicle vt0 nodes: 
0 5 20 15 27 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-1 0 35 0 12 23 10
20-2 13 0 24 9 24 6
20-1 13 0 9 7 36 15
15-1 13 19 0 8 26 9
27-2 0 0 12 13 24 18
27-1 10 0 0 12 19 9
27-3 0 0 0 10 35 12


Vehicle vt0 nodes: 
0 28 22 1 23 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
28-1 13 45 0 12 15 13
22-2 13 32 0 10 13 18
22-1 12 0 16 6 32 11
1-2 0 0 16 10 25 9
1-1 8 0 0 14 29 16
23-1 0 29 7 12 19 11
23-2 0 29 0 13 23 7
23-3 0 0 0 8 29 16


Vehicle vt0 nodes: 
0 30 2 6 0 
Packing_pattern: WALL
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
30-1 0 33 14 15 15 12
2-1 0 33 0 13 21 14
2-3 11 0 0 14 22 15
2-2 0 0 13 10 19 9
6-1 0 0 0 11 33 13


Vehicle vt0 nodes: 
0 3 18 25 9 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
3-3 0 15 20 13 24 6
3-1 0 15 10 10 30 10
3-2 14 0 0 11 15 13
18-3 18 22 0 5 21 13
18-1 0 0 10 14 15 15
18-2 11 22 0 7 29 14
25-1 0 54 0 19 6 7
9-1 0 22 0 11 32 10
9-3 0 16 0 24 6 6
9-2 0 0 0 13 16 10


Vehicle vt0 nodes: 
0 12 26 4 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-3 20 0 0 5 35 12
12-2 6 43 0 19 14 10
12-1 13 0 16 7 34 13
26-2 0 31 17 8 12 11
26-1 0 31 0 6 18 17
26-3 0 0 18 12 16 9
4-1 13 0 0 6 34 16
4-2 0 0 0 13 31 18


Vehicle vt0 nodes: 
0 11 10 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-1 10 0 0 15 24 18
10-1 0 0 0 10 16 11


Vehicle vt0 nodes: 
0 21 29 13 7 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-2 14 45 0 8 15 16
21-1 0 32 21 11 16 9
29-1 0 34 11 14 25 10
29-2 0 32 0 11 25 11
29-3 12 20 11 9 14 17
13-3 9 0 21 8 17 9
13-2 12 22 0 11 23 11
13-1 9 0 8 12 20 13
7-3 12 0 0 11 22 8
7-1 0 0 8 9 25 15
7-2 0 0 0 12 32 8


Vehicle vt0 nodes: 
0 8 19 14 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
8-1 12 13 16 13 20 13
8-2 0 13 12 12 21 12
19-1 13 13 0 10 31 16
19-2 0 13 0 13 21 12
19-3 7 0 0 9 13 17
14-1 0 0 0 7 13 18


Vehicle vt0 nodes: 
0 17 24 16 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 0 34 0 11 24 10
24-1 11 0 11 12 27 15
24-2 11 0 0 10 35 11
24-3 0 19 0 11 15 15
16-2 0 0 10 9 14 8
16-1 0 0 0 10 19 10



Rounds: 5 Idle rounds: 2
Seed: 2037239458
Hard_constraints: 0 0
Time: 725.049
Cost: 612263
