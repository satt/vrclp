Total_Vehicles 
6

Vehicle vt0 nodes: 
0 19 21 17 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
19-2 7 0 18 14 29 7
19-1 7 20 9 6 20 9
21-2 13 20 0 12 34 18
21-1 0 14 9 7 29 14
17-1 0 20 0 13 34 9
17-2 11 0 0 11 20 18
17-3 0 0 0 11 14 16


Vehicle vt0 nodes: 
0 10 7 5 9 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-1 14 34 0 11 21 13
10-3 0 25 19 5 35 7
10-2 15 0 14 6 34 11
7-1 0 25 9 14 28 10
5-1 0 11 9 13 14 18
5-3 0 34 0 12 19 9
5-2 0 13 0 12 21 9
9-3 0 0 8 15 11 10
9-1 15 0 0 6 31 14
9-2 0 0 0 15 13 8


Vehicle vt0 nodes: 
0 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 16 13 8 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
16-1 14 0 11 9 35 16
16-2 6 0 16 6 26 9
13-1 0 27 17 10 28 9
13-2 9 34 0 15 18 7
13-3 0 0 16 6 27 10
8-2 14 0 0 11 34 11
8-3 0 30 0 9 25 17
8-1 0 0 0 14 30 16


Vehicle vt0 nodes: 
0 11 4 3 1 2 6 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-2 0 32 17 10 22 12
11-1 11 10 8 14 34 8
4-1 0 34 0 10 26 17
3-1 0 0 19 10 25 9
1-2 0 10 6 11 22 13
1-1 12 24 0 13 32 8
2-3 0 0 9 20 10 10
2-1 0 10 0 12 24 6
2-2 15 0 0 8 24 8
6-1 0 0 0 15 10 9


Vehicle vt0 nodes: 
0 14 20 18 15 12 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
14-2 0 47 15 15 9 7
14-3 14 15 13 6 32 14
14-1 0 24 15 13 23 15
20-3 13 0 11 6 15 13
20-1 14 23 0 11 29 13
20-2 0 0 11 13 24 13
18-2 12 0 0 12 23 11
18-1 6 29 0 8 31 15
15-1 0 29 0 6 26 15
12-1 0 0 0 12 29 11



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 1269895266
Hard_constraints: 0 0
Time: 172.772
Cost: 444660
