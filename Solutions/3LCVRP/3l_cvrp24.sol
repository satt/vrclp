Total_Vehicles 
16

Vehicle vt0 nodes: 
0 12 72 9 40 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-1 0 31 13 9 21 14
72-2 0 31 0 7 29 13
72-1 12 24 0 10 36 16
9-2 11 0 8 9 17 13
9-1 12 0 0 11 24 8
40-2 0 0 17 11 28 9
40-1 0 0 8 10 31 9
40-3 0 0 0 12 28 8


Vehicle vt0 nodes: 
0 51 16 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
51-1 0 47 6 15 10 12
51-3 15 30 0 6 25 7
51-2 15 0 0 8 30 7
16-3 0 47 0 12 13 6
16-1 0 12 0 15 35 16
16-2 0 0 0 14 12 18


Vehicle vt0 nodes: 
0 48 21 47 5 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
48-1 0 42 9 19 9 14
48-2 0 42 0 23 11 9
21-1 0 27 16 12 14 13
21-3 12 9 7 11 33 12
21-2 0 6 16 10 21 10
47-1 0 0 13 22 6 13
47-2 12 9 0 12 32 7
5-1 0 9 0 12 32 16
5-2 0 0 0 22 9 13


Vehicle vt0 nodes: 
0 17 32 50 44 3 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 17 23 11 6 31 15
32-1 10 23 11 7 29 7
50-2 0 12 12 7 22 9
50-1 10 23 0 12 33 11
44-3 11 0 18 12 23 6
44-2 11 0 10 10 22 8
44-1 0 12 0 10 31 12
3-1 13 0 0 11 19 10
3-2 0 0 10 11 12 10
3-3 0 0 0 13 9 10


Vehicle vt0 nodes: 
0 53 14 59 8 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
53-1 16 23 12 7 36 17
14-3 16 0 0 7 23 13
14-2 0 24 11 13 31 16
14-1 14 24 0 11 34 12
59-1 0 24 0 14 31 11
8-1 7 0 0 9 24 18
8-2 0 0 0 7 23 12


Vehicle vt0 nodes: 
0 4 45 13 54 19 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
4-1 12 29 13 10 26 15
45-1 14 29 0 11 26 13
45-2 0 33 16 12 24 7
45-3 10 0 14 9 24 15
13-1 0 33 8 12 21 8
54-1 10 0 0 12 29 14
19-3 0 33 0 14 22 8
19-2 0 0 17 10 29 11
19-1 0 0 0 8 33 17


Vehicle vt0 nodes: 
0 39 31 25 55 18 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
39-2 12 34 14 10 21 7
39-1 0 32 15 12 27 13
39-3 19 0 0 6 14 17
31-1 12 34 0 8 26 14
25-1 14 0 0 5 34 8
55-1 7 0 16 7 32 12
18-3 0 0 16 7 20 9
18-1 0 29 0 12 27 15
18-2 0 0 0 14 29 16


Vehicle vt0 nodes: 
0 43 41 42 64 22 61 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
43-1 14 27 6 8 16 17
43-2 14 27 0 9 30 6
43-3 20 0 0 5 21 8
41-1 14 0 9 6 27 7
42-1 14 0 0 6 22 9
42-2 0 34 11 14 17 7
64-2 0 34 0 14 26 11
64-1 0 10 16 12 13 12
22-1 0 0 16 14 10 13
61-1 0 0 0 13 34 16


Vehicle vt0 nodes: 
0 65 66 11 35 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
65-1 14 0 20 6 17 10
66-2 0 46 0 22 14 14
66-1 0 27 17 11 14 11
66-3 7 32 0 15 14 17
11-1 0 15 0 7 29 17
35-2 14 0 12 10 32 8
35-1 14 0 0 10 27 12
35-3 0 0 0 14 15 14


Vehicle vt0 nodes: 
0 62 73 1 33 6 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
62-1 17 17 19 6 26 10
73-3 11 18 19 6 30 6
73-1 0 35 0 10 22 10
73-2 11 18 7 11 34 12
1-1 15 0 21 6 14 8
33-1 15 0 14 8 17 7
33-2 0 0 15 15 14 15
33-3 11 18 0 14 31 7
6-1 0 18 0 11 17 17
6-2 12 0 0 13 13 14
6-3 0 0 0 12 18 15


Vehicle vt0 nodes: 
0 68 2 28 74 30 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
68-2 13 31 0 8 20 11
68-1 0 26 7 10 30 17
2-1 0 5 23 14 13 6
28-1 14 0 8 11 30 14
28-3 0 26 0 13 33 7
28-2 0 5 12 14 21 11
74-1 0 5 0 14 16 12
30-1 14 0 0 9 31 8
30-2 0 0 0 14 5 18


Vehicle vt0 nodes: 
0 49 24 56 23 63 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
49-3 0 0 16 8 29 12
49-1 8 44 7 15 15 16
49-2 0 44 7 8 14 8
24-1 12 0 16 8 28 14
24-2 0 44 0 25 13 7
56-1 12 0 0 12 30 16
23-1 0 32 0 24 12 16
63-2 0 0 9 7 31 7
63-1 0 0 0 12 32 9


Vehicle vt0 nodes: 
0 58 10 38 7 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
58-1 10 34 0 15 18 8
10-2 0 0 23 6 27 7
10-1 0 54 0 16 6 13
38-1 9 0 9 9 27 15
38-2 15 0 0 7 34 9
38-3 9 0 0 6 31 9
7-1 0 33 0 10 21 18
7-3 0 0 11 7 33 12
7-2 0 0 0 9 32 11


Vehicle vt0 nodes: 
0 67 46 34 26 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
67-2 15 14 11 6 30 11
67-1 0 48 0 15 11 15
46-2 19 0 0 6 14 12
46-1 16 14 0 7 32 11
34-1 0 14 10 14 26 7
34-3 0 0 10 19 14 16
34-2 0 36 0 16 12 10
26-1 0 0 0 15 36 10


Vehicle vt0 nodes: 
0 36 69 71 70 60 29 75 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
36-2 19 7 9 6 27 7
36-1 0 43 10 14 14 13
36-3 13 7 9 6 17 8
69-1 7 7 15 6 33 7
71-1 0 0 16 14 7 12
70-1 0 43 0 18 13 10
70-2 13 8 0 11 35 9
60-2 0 7 15 7 27 15
60-1 0 8 6 13 35 9
60-3 0 0 9 25 7 7
29-1 0 8 0 12 33 6
75-1 0 0 0 20 8 9


Vehicle vt0 nodes: 
0 27 37 20 15 57 52 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
27-1 9 29 13 14 14 14
37-3 0 27 9 9 26 11
37-1 12 29 0 11 26 13
37-2 18 0 16 7 14 6
20-1 18 0 0 7 29 16
15-1 0 27 0 12 28 9
57-1 0 0 17 15 26 11
52-2 12 0 0 6 23 15
52-1 0 0 0 12 27 17



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 946475189
Hard_constraints: 0 0
Time: 2181.97
Cost: 1162374
