Total_Vehicles 
4

Vehicle vt0 nodes: 
0 7 15 4 14 13 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
7-1 10 34 0 6 26 13
15-1 0 0 17 15 29 11
4-1 0 34 11 5 24 13
14-2 15 0 0 10 33 14
14-1 0 34 0 10 22 11
13-2 0 28 0 15 6 16
13-1 0 0 0 14 28 17


Vehicle vt0 nodes: 
0 20 6 11 10 19 12 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
20-1 13 43 0 8 14 16
6-1 19 0 11 6 27 13
6-2 0 0 22 15 27 6
11-1 0 27 14 11 27 15
11-3 8 0 12 8 20 10
11-2 19 0 0 6 25 11
10-2 0 0 12 8 27 10
10-3 13 27 0 12 16 8
10-1 0 27 0 13 33 14
19-1 0 13 0 19 14 12
12-1 0 0 0 13 13 12


Vehicle vt0 nodes: 
0 2 1 18 16 17 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
2-1 9 37 0 16 14 9
1-1 0 29 16 24 6 12
18-1 0 37 0 9 16 17
18-3 12 0 21 7 19 9
18-2 12 0 7 6 24 14
16-2 0 29 0 20 8 16
16-3 12 0 0 12 19 7
16-1 0 0 9 12 29 18
17-1 0 0 0 12 29 9


Vehicle vt0 nodes: 
0 5 9 3 8 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-2 0 53 0 23 5 16
5-1 0 26 16 22 15 8
5-3 12 31 0 13 21 16
9-1 19 0 0 6 25 14
9-2 0 38 0 9 15 12
9-3 13 0 16 5 26 9
3-1 0 31 0 12 7 16
8-2 6 0 16 7 22 13
8-1 6 0 0 13 31 16
8-3 0 0 0 6 26 12



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 397124941
Hard_constraints: 0 0
Time: 154.651
Cost: 397126
