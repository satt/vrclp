Total_Vehicles 
11

Vehicle vt0 nodes: 
0 6 2 30 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
6-1 0 54 0 19 6 15
2-3 6 31 0 14 23 13
2-1 6 18 0 16 13 12
2-2 0 18 0 6 33 8
30-1 0 0 0 8 18 11


Vehicle vt0 nodes: 
0 17 16 33 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 11 0 19 6 28 11
16-1 11 0 6 5 33 13
33-1 0 35 0 15 24 17
33-2 11 0 0 5 34 6
33-3 0 0 0 11 35 17


Vehicle vt0 nodes: 
0 10 31 12 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-2 14 0 14 5 22 15
10-3 0 52 0 18 6 7
10-1 0 10 9 6 29 10
31-1 14 0 0 10 19 14
31-2 0 37 0 24 15 7
12-2 0 10 0 6 27 9
12-1 0 0 0 14 10 12


Vehicle vt0 nodes: 
0 8 19 14 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
8-1 0 39 0 24 12 17
19-2 0 33 0 19 6 10
19-1 10 0 0 10 33 15
14-1 0 0 0 10 31 7


Vehicle vt0 nodes: 
0 25 18 24 3 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
25-1 14 0 0 5 21 13
18-1 12 35 0 12 14 18
24-1 0 35 0 12 23 16
3-1 0 0 0 14 35 13


Vehicle vt0 nodes: 
0 28 22 1 23 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
28-1 10 10 20 5 29 7
22-1 10 11 8 13 32 12
1-2 0 0 13 15 10 14
1-1 0 44 0 12 10 8
23-2 10 11 0 12 33 8
23-3 0 11 0 10 26 6
23-1 0 0 0 25 11 13


Vehicle vt0 nodes: 
0 26 34 4 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
26-2 17 25 0 6 16 12
26-1 11 25 0 6 35 12
26-3 11 0 10 9 20 13
34-1 0 0 16 7 25 9
4-1 11 0 0 7 25 10
4-2 0 0 0 11 34 16


Vehicle vt0 nodes: 
0 32 9 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
32-1 0 25 0 13 24 13
9-2 0 0 11 6 17 13
9-1 0 0 0 13 25 11


Vehicle vt0 nodes: 
0 21 5 29 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 8 30 16 15 28 7
5-1 8 30 0 15 23 16
5-2 0 30 0 8 25 7
29-2 15 0 0 9 13 9
29-1 0 7 0 11 23 11
29-3 0 0 0 15 7 7


Vehicle vt0 nodes: 
0 20 15 13 27 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
20-1 0 47 0 15 11 13
20-2 0 17 20 5 28 8
15-2 10 34 0 12 13 14
15-3 0 20 9 7 24 11
15-1 10 14 11 9 20 11
13-1 0 20 0 10 24 9
13-2 8 0 11 8 14 13
27-1 10 0 0 11 31 11
27-3 0 0 10 8 17 12
27-2 0 0 0 10 20 10


Vehicle vt0 nodes: 
0 35 11 7 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
35-2 19 26 0 6 16 7
35-3 14 26 0 5 15 6
35-1 0 26 11 14 34 17
11-2 0 26 0 13 28 11
11-1 0 13 0 24 13 18
7-1 0 0 0 13 13 15



Rounds: 3 Idle rounds: 1
Kick Rounds: 2 Kick improving rounds: 1
Seed: 722672516
Hard_constraints: 0 0
Time: 484.911
Cost: 698626
