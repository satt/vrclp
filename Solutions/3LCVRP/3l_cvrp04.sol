Total_Vehicles 
6

Vehicle vt0 nodes: 
0 5 20 19 11 6 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-2 13 25 16 7 33 9
5-1 13 14 0 8 36 16
20-1 0 0 21 24 9 8
19-2 13 0 10 10 14 11
19-1 0 0 14 13 13 7
11-2 0 25 13 7 32 11
11-1 0 25 0 13 34 13
11-3 12 0 0 12 11 10
6-1 6 0 0 6 19 14
6-2 0 0 0 6 25 14


Vehicle vt0 nodes: 
0 2 1 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
2-1 15 27 0 10 30 12
1-2 0 27 0 15 30 10
1-3 0 19 0 18 8 17
1-1 0 0 0 6 19 17


Vehicle vt0 nodes: 
0 16 18 10 12 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
16-1 14 0 16 10 13 9
18-3 0 0 14 7 15 6
18-2 14 0 10 10 20 6
18-1 0 30 0 14 15 11
10-2 0 0 7 6 16 7
10-1 12 0 0 12 30 10
12-1 0 0 0 12 20 7


Vehicle vt0 nodes: 
0 13 14 15 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-2 10 30 15 8 19 13
13-1 0 29 12 8 29 18
14-3 10 30 0 15 16 15
14-2 0 29 0 10 22 12
14-1 12 0 9 12 30 16
15-2 12 0 0 13 30 9
15-3 0 0 16 10 22 12
15-1 0 0 0 12 29 16


Vehicle vt0 nodes: 
0 17 8 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 7 0 0 8 16 8
8-1 0 0 0 7 32 14


Vehicle vt0 nodes: 
0 4 3 9 7 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
4-1 0 21 11 12 22 9
3-1 0 21 0 14 34 11
3-2 14 15 0 6 29 16
9-1 14 0 0 7 15 16
7-1 0 0 0 14 21 18



Rounds: 2 Idle rounds: 1
Seed: 2101976079
Hard_constraints: 0 0
Time: 151.729
Cost: 437201
