Total_Vehicles 
8

Vehicle vt0 nodes: 
0 20 22 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
20-1 14 26 0 7 25 14
20-2 8 26 0 6 30 6
20-3 0 26 0 8 34 9
22-2 0 14 0 22 12 12
22-1 0 0 0 25 14 16


Vehicle vt0 nodes: 
0 19 6 24 25 3 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
19-1 12 34 10 11 18 17
6-1 13 0 17 6 26 7
6-3 0 0 18 12 21 8
6-2 0 27 12 12 29 13
24-2 11 34 0 12 15 10
24-3 18 15 0 7 16 7
24-1 0 27 0 11 30 12
25-2 0 13 0 13 14 18
25-1 18 0 0 7 15 13
3-2 13 0 0 5 34 17
3-1 0 0 0 13 13 18


Vehicle vt0 nodes: 
0 21 10 23 18 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 13 23 17 8 15 13
10-3 13 0 17 8 23 12
10-2 0 40 0 10 16 16
10-1 14 32 0 10 26 17
23-1 0 33 0 14 7 15
18-3 15 0 0 6 32 17
18-2 0 0 13 13 33 9
18-1 0 0 0 15 31 13


Vehicle vt0 nodes: 
0 4 1 5 2 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
4-1 12 25 15 6 16 13
1-3 0 25 15 12 34 11
1-1 0 49 0 23 11 15
1-2 11 0 15 7 25 6
5-3 0 34 0 18 15 15
5-2 11 0 7 10 33 8
5-1 11 0 0 13 34 7
2-2 0 0 12 7 15 9
2-1 0 0 0 11 28 12


Vehicle vt0 nodes: 
0 11 17 7 13 16 15 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-2 0 52 17 22 6 9
11-1 0 52 0 24 8 17
17-1 17 18 17 8 30 13
17-2 12 18 17 5 34 9
7-2 0 18 17 12 30 7
7-1 8 18 0 15 30 17
13-1 0 18 0 8 34 17
16-1 0 9 0 24 9 14
15-1 0 0 11 21 9 13
15-2 0 0 0 17 9 11


Vehicle vt0 nodes: 
0 0 
Packing_pattern: WALL
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 9 14 8 12 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
9-2 11 24 11 8 29 12
9-3 0 23 16 11 34 12
9-1 19 19 11 6 32 17
14-1 0 23 8 9 34 8
14-2 9 0 14 10 21 6
8-2 0 0 11 9 23 18
8-1 10 24 0 15 30 11
12-1 0 19 0 10 34 8
12-2 10 0 0 8 24 14
12-3 0 0 0 10 19 11


Vehicle vt0 nodes: 
0 26 28 27 29 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
26-2 0 46 16 22 5 11
26-1 0 46 7 25 11 9
28-1 0 26 16 25 15 14
28-3 0 46 0 21 13 7
28-2 14 28 0 8 18 16
27-1 0 18 0 14 22 16
29-3 15 0 11 9 26 8
29-2 15 0 0 7 28 11
29-1 0 0 0 15 18 15



Rounds: 4 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 861471135
Hard_constraints: 0 0
Time: 623.175
Cost: 823163
