Total_Vehicles 
8

Vehicle vt0 nodes: 
0 32 1 17 13 16 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
32-1 0 48 0 24 11 15
1-1 8 27 14 14 21 12
1-2 0 15 11 8 18 8
17-1 8 27 0 12 21 14
13-3 9 0 21 10 21 7
13-2 9 0 12 11 27 9
13-1 0 15 0 8 32 11
16-3 0 0 13 9 13 6
16-1 9 0 0 11 22 12
16-2 0 0 0 9 15 13


Vehicle vt0 nodes: 
0 31 6 12 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
31-1 0 40 0 14 20 10
31-2 0 14 24 14 18 6
6-3 15 14 7 7 32 10
6-2 0 0 15 21 10 12
6-1 0 14 17 12 26 7
12-2 15 14 0 7 35 7
12-1 0 14 0 15 26 17
12-3 0 0 0 25 14 15


Vehicle vt0 nodes: 
0 8 24 14 15 21 10 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
8-1 15 0 20 7 31 9
24-3 0 22 22 12 14 7
24-1 0 39 10 12 20 17
24-2 14 28 12 9 22 7
14-1 0 22 10 14 17 12
14-2 15 0 9 7 28 11
15-2 14 28 0 8 24 12
15-3 0 0 10 15 22 15
15-1 0 28 0 14 26 10
21-1 15 0 0 9 28 9
10-1 0 0 0 15 28 10


Vehicle vt0 nodes: 
0 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 4 26 18 27 28 2 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
4-2 7 26 6 15 24 18
4-3 0 17 14 6 30 15
4-1 7 26 0 12 27 6
26-1 0 17 0 7 31 14
18-1 12 0 15 11 23 13
27-1 0 0 18 12 17 8
28-1 12 0 0 9 26 15
2-1 0 0 0 12 16 18


Vehicle vt0 nodes: 
0 11 9 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-1 13 0 0 10 19 16
11-2 9 25 0 14 31 12
9-3 0 25 0 9 19 13
9-2 0 0 10 11 25 9
9-1 0 0 0 13 24 10


Vehicle vt0 nodes: 
0 22 29 30 7 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
22-1 0 44 10 12 15 12
22-3 11 44 0 14 14 7
22-2 0 44 0 11 13 10
29-2 0 21 0 7 19 11
29-1 10 15 9 14 29 16
29-3 0 0 18 10 13 11
30-1 10 18 0 14 20 9
30-3 10 0 9 13 15 17
30-2 10 0 0 12 18 9
7-1 0 0 0 10 21 18


Vehicle vt0 nodes: 
0 5 3 20 25 19 23 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-1 11 29 6 7 31 9
3-1 0 22 15 10 20 7
20-1 11 29 0 13 31 6
20-2 0 0 17 7 22 6
25-2 10 0 13 6 29 15
25-1 11 0 0 10 28 13
25-3 0 21 8 10 25 7
19-1 0 21 0 11 33 8
23-1 0 0 0 9 21 17



Rounds: 4 Idle rounds: 1
Seed: 852369748
Hard_constraints: 0 0
Time: 504.533
Cost: 2712638
