Total_Vehicles 
18

Vehicle vt0 nodes: 
0 31 25 55 18 50 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
31-2 14 0 12 8 23 10
31-1 14 37 9 11 19 9
25-1 14 24 9 11 13 11
25-2 0 39 0 5 19 10
55-1 0 0 15 12 36 15
18-2 14 24 0 9 34 9
18-1 14 0 0 11 24 12
50-2 0 27 0 14 12 12
50-1 0 0 0 14 27 15


Vehicle vt0 nodes: 
0 62 22 64 42 41 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
62-1 10 39 15 15 20 9
22-3 0 43 15 10 14 9
22-2 0 45 7 21 14 8
22-1 7 10 15 14 29 15
64-1 0 45 0 20 15 7
42-2 0 10 15 7 33 15
42-3 0 10 7 10 25 8
42-1 11 10 0 13 35 15
41-2 0 0 18 17 10 10
41-3 0 8 0 11 21 7
41-1 0 0 0 25 8 18


Vehicle vt0 nodes: 
0 68 2 73 33 6 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
68-2 0 35 16 15 11 10
68-1 15 14 7 8 27 15
2-1 15 14 0 9 34 7
73-1 15 0 0 9 14 9
73-2 7 0 15 8 25 15
73-3 0 0 15 7 29 14
33-1 0 35 0 12 16 16
6-2 0 0 9 14 30 6
6-1 0 0 0 12 35 9


Vehicle vt0 nodes: 
0 27 20 70 37 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
27-2 15 26 15 6 28 9
27-1 0 26 16 15 18 14
20-1 12 26 0 13 34 15
20-2 13 0 13 8 26 16
70-1 0 14 0 12 31 16
70-3 0 0 16 12 13 12
70-2 13 0 0 12 23 13
37-1 0 0 0 13 14 16


Vehicle vt0 nodes: 
0 12 26 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-1 13 0 0 11 19 17
12-2 0 28 0 14 32 15
26-2 0 0 16 6 17 8
26-1 0 0 0 13 28 16


Vehicle vt0 nodes: 
0 45 4 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
45-1 15 0 0 7 34 14
45-2 0 0 15 14 35 12
4-1 0 31 0 15 26 7
4-2 0 0 0 13 31 15


Vehicle vt0 nodes: 
0 10 38 65 66 67 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-2 14 30 18 9 27 11
10-1 0 46 14 14 13 16
38-2 14 30 0 8 29 18
38-1 0 19 14 10 27 13
65-1 0 23 0 14 35 14
65-2 17 0 15 5 30 7
66-1 17 0 0 8 29 15
66-2 0 0 13 13 15 17
67-2 12 0 0 5 23 11
67-1 0 0 0 12 19 13


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 13 54 19 14 59 11 53 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-2 15 0 11 7 18 13
13-1 11 28 20 14 20 10
54-1 8 55 0 14 5 12
19-1 0 28 13 11 27 15
14-1 0 34 0 8 24 13
59-1 12 28 7 13 20 13
59-2 12 28 0 11 27 7
59-3 0 0 13 15 28 10
11-1 12 0 0 10 28 11
53-1 0 0 0 12 34 13


Vehicle vt0 nodes: 
0 52 57 15 34 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
52-2 0 24 15 8 27 14
52-1 16 35 0 8 22 7
57-2 15 0 9 8 35 18
57-1 11 32 0 5 21 9
57-3 0 26 0 11 28 15
15-2 15 0 0 10 32 9
15-1 0 0 11 15 24 17
34-1 0 0 0 14 26 11


Vehicle vt0 nodes: 
0 3 24 49 16 51 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
3-2 11 46 13 13 12 17
3-1 11 20 13 10 26 11
3-3 0 22 14 6 22 10
24-1 11 20 0 13 35 13
24-2 0 22 0 11 27 14
49-2 8 8 16 13 12 8
49-1 8 0 16 17 8 11
16-2 0 0 15 8 22 9
16-1 8 0 0 15 20 16
16-3 0 0 8 7 22 7
51-1 0 0 0 8 20 8


Vehicle vt0 nodes: 
0 28 61 74 30 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
28-1 12 0 17 6 34 11
61-2 8 44 0 14 13 13
61-1 12 0 0 13 35 17
74-1 8 36 0 13 8 10
74-2 0 32 15 8 25 6
30-2 0 0 11 8 32 14
30-3 0 36 0 8 23 15
30-1 0 0 0 12 36 11


Vehicle vt0 nodes: 
0 29 5 36 47 48 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
29-1 15 24 16 6 29 11
5-2 15 26 0 7 27 16
5-1 0 24 14 15 16 13
36-2 0 27 0 14 24 14
36-3 8 0 14 15 24 9
36-1 12 0 0 13 26 14
47-1 0 0 20 8 15 10
48-2 0 0 14 8 19 6
48-1 0 0 0 12 27 14


Vehicle vt0 nodes: 
0 46 8 35 7 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
46-2 0 0 17 8 27 8
46-1 0 40 0 19 15 11
8-1 11 18 0 12 22 9
35-2 11 0 15 14 16 14
35-1 11 0 0 12 18 15
7-2 0 0 10 6 35 7
7-1 0 0 0 11 33 10


Vehicle vt0 nodes: 
0 63 23 56 43 1 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
63-2 0 49 12 21 8 9
63-1 14 0 14 8 12 11
23-1 0 49 0 21 11 12
23-2 15 13 0 6 29 12
56-2 0 17 16 13 32 8
56-1 0 17 0 15 32 16
43-1 0 0 14 14 17 16
1-1 0 0 0 20 13 14


Vehicle vt0 nodes: 
0 44 32 72 58 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
44-2 0 45 17 13 14 13
44-1 0 45 0 12 13 17
44-3 0 32 12 25 6 9
32-2 19 11 19 5 18 11
32-1 7 11 19 12 14 11
72-3 7 0 19 14 11 7
72-2 0 32 0 20 13 12
72-1 10 0 7 15 29 12
58-3 10 0 0 12 32 7
58-2 0 0 12 7 32 15
58-1 0 0 0 10 29 12


Vehicle vt0 nodes: 
0 17 40 9 39 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 0 49 0 25 11 12
17-2 6 9 18 9 29 11
40-1 6 12 10 9 30 8
40-2 0 44 0 16 5 14
9-2 6 12 0 15 30 10
9-1 0 12 0 6 32 17
39-2 0 0 13 21 9 17
39-1 0 0 0 23 12 13


Vehicle vt0 nodes: 
0 75 21 69 71 60 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
75-1 11 23 10 14 13 15
75-2 13 37 0 5 20 6
21-1 13 23 0 12 14 10
69-3 0 46 10 8 14 12
69-1 5 27 10 6 18 17
69-2 9 0 18 12 21 7
71-1 9 0 0 14 23 18
71-3 0 27 10 5 19 17
71-2 0 27 0 13 31 10
60-2 0 0 17 9 17 8
60-1 0 0 0 9 27 17



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 1105527570
Hard_constraints: 0 0
Time: 1917.18
Cost: 1188506
