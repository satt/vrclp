Total_Vehicles 
17

Vehicle vt0 nodes: 
0 62 28 61 21 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
62-1 12 0 16 9 32 8
62-3 13 34 14 9 14 8
62-2 5 34 15 8 19 14
28-2 0 34 15 5 26 12
28-1 12 34 0 12 21 14
61-1 0 26 0 12 30 15
61-2 13 0 0 10 34 16
21-3 0 0 18 12 12 8
21-2 0 0 8 12 12 10
21-1 0 0 0 13 26 8


Vehicle vt0 nodes: 
0 3 49 24 18 55 25 9 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
3-2 12 31 14 7 26 16
3-1 12 31 6 12 26 8
49-3 18 0 19 7 17 11
49-2 12 31 0 12 28 6
49-1 18 0 8 7 30 11
24-2 0 33 16 12 12 11
24-3 18 0 0 7 23 8
24-1 0 33 0 10 27 16
18-1 6 0 13 12 31 16
55-1 10 0 0 8 25 13
25-1 0 0 13 6 33 13
9-1 0 0 0 10 25 13


Vehicle vt0 nodes: 
0 12 40 17 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-1 0 30 16 12 27 12
12-2 0 53 0 23 7 9
40-2 12 0 0 7 30 17
40-1 0 22 0 12 31 16
17-1 0 0 0 11 22 9


Vehicle vt0 nodes: 
0 57 15 37 60 29 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
57-3 10 35 16 14 24 10
57-2 0 35 11 10 18 12
57-1 0 35 0 11 22 11
15-1 12 0 18 12 34 9
37-2 12 33 0 11 27 16
37-1 0 0 7 12 35 15
60-1 14 0 0 11 33 18
29-1 0 0 0 14 32 7


Vehicle vt0 nodes: 
0 67 53 35 8 34 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
67-2 20 0 6 5 26 9
67-1 8 0 14 7 35 16
53-1 0 36 13 9 16 7
53-2 0 0 14 8 36 16
35-2 10 39 9 6 21 7
35-1 10 0 6 10 26 8
8-2 8 39 0 12 21 9
8-1 0 35 0 8 18 13
34-2 10 27 0 13 12 11
34-3 10 0 0 14 27 6
34-1 0 0 0 10 35 14


Vehicle vt0 nodes: 
0 4 27 45 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
4-3 17 23 8 6 32 15
4-2 12 0 16 13 23 12
4-1 12 0 0 10 23 16
27-1 9 28 8 8 28 17
27-3 0 25 13 7 35 11
27-2 9 28 0 14 31 8
45-1 0 0 12 12 25 11
45-2 0 28 0 9 28 13
45-3 0 0 0 12 28 12


Vehicle vt0 nodes: 
0 16 23 56 41 73 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
16-1 14 24 8 10 35 12
23-1 14 24 0 9 34 8
56-3 14 0 9 9 18 10
56-2 0 28 17 13 27 10
56-1 0 35 0 13 16 17
41-1 0 0 17 14 28 8
73-2 14 0 0 10 24 9
73-1 0 0 0 14 35 17


Vehicle vt0 nodes: 
0 32 50 44 51 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
32-1 9 0 23 9 25 7
50-1 9 0 15 15 36 8
50-3 18 31 0 6 19 11
50-2 0 0 15 9 31 10
44-2 6 31 0 12 17 11
44-1 11 0 0 12 31 15
44-3 0 48 0 16 8 8
51-2 0 19 0 6 29 15
51-1 0 0 0 11 19 15


Vehicle vt0 nodes: 
0 1 33 6 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
1-1 0 13 21 12 32 8
33-3 0 45 0 19 12 13
33-1 14 14 0 11 31 12
33-2 0 13 13 12 31 8
6-1 0 14 0 14 31 13
6-2 8 0 0 10 14 12
6-3 0 0 0 8 13 9


Vehicle vt0 nodes: 
0 63 43 42 64 22 2 68 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
63-1 0 41 17 18 11 12
63-2 0 41 10 16 15 7
43-1 20 0 0 5 30 8
42-1 14 35 0 10 22 9
64-2 0 16 13 12 25 16
64-1 0 36 0 14 23 10
22-1 0 16 0 13 20 13
2-2 0 0 18 6 16 9
2-1 13 0 16 6 35 7
68-1 13 0 0 7 33 16
68-2 0 0 7 9 16 11
68-3 0 0 0 13 16 7


Vehicle vt0 nodes: 
0 5 47 36 69 71 70 20 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-1 0 36 19 6 17 11
47-3 17 0 16 5 29 10
47-2 11 36 10 6 18 15
47-1 11 33 0 8 24 10
36-1 0 36 8 8 23 11
69-2 0 12 24 15 21 6
69-1 17 0 0 5 32 16
71-2 0 0 24 7 12 6
71-1 10 0 15 7 33 9
70-1 10 0 0 6 30 15
20-3 0 36 0 11 21 8
20-2 0 0 17 10 36 7
20-1 0 0 0 9 36 17


Vehicle vt0 nodes: 
0 58 10 31 72 39 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
58-1 10 27 11 10 30 14
58-2 6 0 19 16 7 7
10-1 0 51 0 25 8 11
31-2 10 27 0 14 24 11
31-1 10 10 0 14 17 16
31-3 0 25 10 10 20 9
72-1 6 0 10 19 10 9
39-3 0 0 10 6 25 17
39-1 0 9 0 10 34 10
39-2 0 0 0 22 9 10


Vehicle vt0 nodes: 
0 46 54 13 52 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
46-3 17 36 10 6 20 9
46-2 10 36 10 7 18 8
46-1 10 36 0 13 19 10
54-1 9 0 15 9 17 13
13-2 0 19 13 10 33 11
13-1 10 17 0 12 19 17
52-1 0 19 0 10 36 13
52-2 9 0 0 14 17 15
52-3 0 0 0 9 19 16


Vehicle vt0 nodes: 
0 30 48 74 75 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
30-2 0 34 17 16 8 13
30-1 8 0 19 13 34 7
48-1 0 43 0 21 11 17
48-2 0 34 0 12 9 17
48-3 0 0 18 8 20 10
74-2 8 0 13 13 34 6
74-1 8 0 0 13 33 13
75-1 0 0 0 8 33 18


Vehicle vt0 nodes: 
0 38 65 66 11 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
38-2 8 22 12 17 13 17
38-1 8 0 12 12 22 15
65-1 0 36 13 18 8 9
66-3 17 34 0 5 23 10
66-1 8 0 0 14 34 12
66-2 0 0 17 8 34 9
11-3 0 45 0 12 13 11
11-2 0 36 0 17 9 13
11-1 0 0 0 6 36 17


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 19 59 14 7 26 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
19-1 0 31 13 14 20 13
19-2 18 28 0 7 30 11
59-2 10 0 20 9 19 7
59-1 0 31 0 13 28 13
14-1 0 19 10 9 12 16
7-1 11 0 6 7 31 14
7-2 11 0 0 13 28 6
26-3 0 0 24 8 19 6
26-2 0 0 10 10 17 14
26-1 0 0 0 11 30 10



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 1409876980
Hard_constraints: 0 0
Time: 2276.44
Cost: 1149333
