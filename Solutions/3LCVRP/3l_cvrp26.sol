Total_Vehicles 
26

Vehicle vt0 nodes: 
0 90 81 73 70 71 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
90-1 12 25 14 6 32 16
81-1 12 25 0 10 28 14
73-1 0 23 8 9 33 17
73-3 15 0 15 8 25 14
73-2 8 0 14 6 23 8
70-1 0 20 0 12 35 8
70-2 15 0 0 9 21 15
71-2 0 0 14 8 20 13
71-1 0 0 0 15 18 14


Vehicle vt0 nodes: 
0 87 83 82 91 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
87-1 0 37 11 9 21 18
87-2 0 8 22 9 29 7
83-1 0 12 11 14 23 11
83-2 15 8 7 7 35 16
82-1 0 29 0 13 28 11
82-2 0 12 0 15 17 11
91-2 15 8 0 9 35 7
91-1 0 0 12 20 8 17
91-3 0 0 0 15 12 12


Vehicle vt0 nodes: 
0 47 42 41 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
47-2 14 0 0 10 15 14
47-1 11 30 16 7 30 13
47-3 5 0 17 7 23 12
42-2 0 25 17 11 19 10
42-1 9 30 0 14 24 16
41-2 0 30 0 9 19 17
41-3 0 0 17 5 25 8
41-1 0 0 0 14 30 17


Vehicle vt0 nodes: 
0 7 3 4 2 75 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
7-2 0 50 18 17 8 12
7-1 0 29 18 14 21 12
7-3 14 13 15 7 35 14
3-2 0 29 9 14 30 9
3-1 14 20 0 11 35 15
4-1 14 0 10 7 13 15
2-2 0 29 0 11 31 9
2-3 14 0 0 10 20 10
2-1 0 0 11 14 29 15
75-1 0 0 0 13 24 11


Vehicle vt0 nodes: 
0 86 84 85 88 89 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
86-2 8 30 15 14 22 11
86-1 0 30 15 8 30 14
84-3 17 0 15 7 19 9
84-1 14 36 0 11 16 15
84-2 17 0 9 8 29 6
85-2 0 0 15 6 27 12
85-1 0 33 0 14 24 15
88-1 17 0 0 8 36 9
88-3 6 0 8 11 30 18
88-2 6 0 0 9 33 8
89-1 0 0 0 6 31 15


Vehicle vt0 nodes: 
0 6 9 11 10 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
6-1 0 34 16 10 15 11
9-1 13 34 17 10 22 7
9-2 0 0 17 13 34 11
11-1 13 31 0 12 20 17
11-3 13 0 0 5 31 17
11-2 0 47 0 6 13 11
10-2 0 34 0 13 13 16
10-1 0 0 0 13 34 17


Vehicle vt0 nodes: 
0 68 53 54 64 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
68-1 14 7 18 6 22 12
53-2 0 41 16 13 17 10
53-1 14 7 0 7 35 18
54-1 0 41 0 14 14 16
54-3 0 7 14 14 34 9
54-2 0 7 6 12 32 8
64-1 0 7 0 11 30 6
64-3 0 0 14 15 7 7
64-2 0 0 0 19 6 14


Vehicle vt0 nodes: 
0 49 52 50 51 48 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
49-1 13 18 9 10 33 17
52-1 13 0 9 7 18 8
50-2 13 31 0 8 29 9
50-3 13 0 0 7 31 9
50-1 0 35 10 12 16 18
51-1 0 35 0 13 18 10
48-1 0 0 16 10 35 13
48-3 0 18 0 13 13 16
48-2 0 0 0 9 18 16


Vehicle vt0 nodes: 
0 65 62 74 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
65-3 14 17 9 11 29 17
65-2 9 0 18 13 13 10
65-1 0 28 10 14 23 13
62-2 12 17 0 12 35 9
62-3 0 31 0 12 23 10
62-1 9 0 0 15 17 18
74-2 0 0 10 9 28 16
74-1 0 0 0 8 31 10


Vehicle vt0 nodes: 
0 67 72 61 69 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
67-1 12 19 14 13 23 16
72-1 12 22 0 12 32 14
72-2 12 0 13 7 19 12
61-1 0 0 19 12 23 8
61-2 0 36 0 7 17 17
61-3 11 0 0 14 22 13
69-2 0 0 11 10 25 8
69-1 0 0 0 11 36 11


Vehicle vt0 nodes: 
0 98 96 95 92 93 94 1 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
98-1 17 16 0 8 14 18
98-2 17 0 0 8 16 15
96-1 10 32 9 12 24 14
96-2 10 0 12 7 30 17
95-1 10 32 0 11 28 9
92-1 10 0 0 6 32 12
93-2 0 21 13 9 32 12
93-1 0 0 14 6 21 15
94-1 0 21 0 10 33 13
1-1 0 0 0 9 21 14


Vehicle vt0 nodes: 
0 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 33 35 31 32 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
33-3 0 36 6 5 24 10
33-1 5 0 24 13 36 6
33-2 5 33 8 13 12 16
35-1 5 33 0 20 15 8
35-2 6 0 12 13 33 12
31-2 0 0 11 6 30 10
31-1 0 30 0 5 28 6
32-1 7 0 0 15 33 12
32-2 0 0 0 7 30 11


Vehicle vt0 nodes: 
0 29 34 36 39 38 37 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
29-1 12 34 14 11 16 15
29-2 0 29 14 6 17 6
34-1 12 34 0 13 26 14
36-1 5 34 0 7 24 11
39-2 0 30 0 5 20 14
39-3 10 18 0 12 16 16
39-1 0 0 10 10 29 16
38-1 15 0 0 7 18 14
37-2 9 0 0 6 16 10
37-1 0 0 0 9 30 10


Vehicle vt0 nodes: 
0 12 14 16 15 13 20 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-1 13 12 22 8 16 6
14-2 5 44 0 18 14 15
14-3 11 0 16 14 12 14
14-1 0 35 0 5 14 10
16-2 13 13 7 8 31 15
16-1 18 13 0 5 30 7
15-2 13 13 0 5 29 7
15-1 13 0 0 12 13 16
13-2 0 0 21 11 22 7
13-1 0 0 10 9 31 11
20-1 0 0 0 13 35 10


Vehicle vt0 nodes: 
0 23 26 27 24 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
23-1 6 48 0 19 9 7
26-1 0 34 0 6 20 9
27-2 7 12 23 11 32 7
27-1 7 12 12 14 36 11
27-3 0 0 16 7 34 10
24-1 7 12 0 14 35 12
24-3 7 0 0 13 12 7
24-2 0 0 0 7 31 16


Vehicle vt0 nodes: 
0 55 56 58 60 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
55-1 0 19 17 12 16 10
56-3 0 6 17 8 13 11
56-2 18 0 17 5 24 11
56-1 0 0 17 18 6 10
58-2 0 50 0 19 7 16
58-1 8 29 0 15 21 17
60-2 0 12 0 8 31 17
60-1 11 0 0 14 29 17
60-3 0 0 0 11 12 17


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 44 45 59 57 66 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
44-2 9 33 16 12 21 9
44-1 0 28 16 9 28 9
45-1 7 0 17 14 28 7
59-3 0 0 16 7 20 10
59-1 5 33 0 13 25 16
59-2 0 21 0 5 31 16
57-1 10 0 0 14 33 17
66-1 0 0 0 10 21 16


Vehicle vt0 nodes: 
0 40 46 43 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
40-1 0 14 14 13 35 14
40-3 0 50 0 18 8 10
40-2 13 17 11 11 30 11
46-2 13 17 0 11 33 11
46-1 0 14 0 13 31 14
46-3 19 0 0 6 17 18
43-1 0 0 0 19 14 15


Vehicle vt0 nodes: 
0 5 21 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-2 15 5 12 7 30 16
5-1 9 5 12 6 33 18
5-3 6 0 12 15 5 8
21-1 9 0 0 14 33 12
21-2 0 0 9 6 29 17
21-3 0 0 0 9 33 9


Vehicle vt0 nodes: 
0 63 80 79 77 78 76 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
63-1 0 32 15 9 14 14
80-1 0 32 0 7 27 15
79-2 0 16 16 10 16 13
79-1 11 17 11 14 36 16
77-1 11 17 0 14 34 11
78-1 0 16 0 11 13 16
78-2 14 0 0 6 17 18
76-2 0 0 17 14 9 10
76-1 0 0 0 12 16 17


Vehicle vt0 nodes: 
0 8 100 97 99 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
8-2 0 38 15 8 13 11
8-1 9 31 10 12 26 14
8-3 9 17 10 14 14 7
100-1 9 24 0 14 34 10
100-3 0 24 15 9 14 11
100-2 0 24 0 8 32 15
97-1 0 0 10 14 17 16
97-2 6 0 0 12 24 10
99-1 0 0 0 6 15 10


Vehicle vt0 nodes: 
0 28 30 18 19 17 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
28-1 15 0 16 9 36 11
28-2 0 0 19 12 34 9
30-1 15 0 0 8 35 16
18-1 0 0 11 15 36 8
19-1 14 38 0 9 17 13
19-2 0 38 0 14 22 12
17-2 0 32 0 15 6 10
17-1 0 0 0 15 32 11


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 22 25 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
22-2 7 0 15 14 36 10
22-3 0 26 14 7 23 9
22-1 0 27 0 15 29 14
25-2 20 0 0 5 34 15
25-1 7 0 0 13 27 15
25-3 0 0 0 7 26 14



Rounds: 4 Idle rounds: 1
Kick Rounds: 2 Kick improving rounds: 1
Seed: 1097555935
Hard_constraints: 0 0
Time: 6533.87
Cost: 1609879
