Total_Vehicles 
17

Vehicle vt0 nodes: 
0 58 38 11 35 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
58-2 0 17 18 5 34 12
58-3 12 37 0 11 23 18
58-1 0 17 10 11 35 8
38-2 0 37 0 12 18 10
38-3 15 0 14 6 35 9
38-1 0 17 0 14 20 10
11-1 15 0 0 8 30 14
35-2 0 0 17 14 17 10
35-1 0 0 0 15 15 17


Vehicle vt0 nodes: 
0 9 39 31 25 55 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
9-1 19 0 0 6 19 10
39-2 10 49 0 15 6 9
39-1 6 34 15 18 13 12
31-3 10 35 0 11 14 15
31-1 9 0 18 10 33 8
31-2 11 0 9 7 28 9
25-1 11 0 0 6 35 9
25-3 0 0 18 9 27 10
25-2 0 34 15 6 17 14
55-2 0 34 0 10 23 15
55-1 0 0 0 11 34 18


Vehicle vt0 nodes: 
0 20 70 60 71 69 28 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
20-1 13 0 15 6 26 8
20-2 13 0 0 11 25 15
70-1 12 26 13 8 20 13
60-1 0 26 23 8 16 7
71-3 0 54 0 18 5 8
71-1 12 26 0 13 27 13
71-2 0 26 10 12 21 13
69-1 0 26 0 11 28 10
69-2 0 0 10 13 26 14
28-1 0 0 0 11 24 10


Vehicle vt0 nodes: 
0 12 40 17 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-3 15 21 17 8 27 10
12-1 15 27 0 10 29 17
12-2 0 28 0 14 22 10
40-2 6 0 15 9 28 7
40-1 0 0 14 6 21 16
17-1 8 0 0 8 27 15
17-2 0 0 0 8 16 14


Vehicle vt0 nodes: 
0 26 67 75 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
26-3 8 0 23 7 29 6
26-1 8 0 13 13 34 10
26-2 0 45 0 22 9 11
67-1 0 34 0 23 11 15
67-2 0 0 14 8 17 13
67-3 0 0 7 8 18 7
75-2 10 0 0 15 34 13
75-1 0 0 0 10 30 7


Vehicle vt0 nodes: 
0 32 50 24 18 44 3 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
32-2 0 54 0 21 6 16
32-1 11 19 13 11 33 16
50-2 7 0 16 18 13 14
50-1 11 19 6 9 35 7
24-1 11 19 0 14 35 6
18-1 7 0 0 15 19 16
44-1 0 21 8 11 21 12
44-2 0 21 0 10 32 8
44-3 0 0 16 7 13 9
3-1 0 0 0 7 21 16


Vehicle vt0 nodes: 
0 30 74 21 61 62 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
30-2 13 34 11 9 15 7
30-1 14 24 0 11 27 11
74-3 0 35 14 7 22 7
74-2 14 0 8 6 18 18
74-1 0 51 0 18 9 14
21-1 0 0 17 12 35 11
61-3 0 34 0 13 17 14
61-2 0 0 8 14 34 9
61-1 12 0 0 7 24 8
62-1 0 0 0 12 33 8


Vehicle vt0 nodes: 
0 6 73 22 64 56 23 49 16 51 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
6-1 16 30 7 5 28 9
6-2 14 12 24 11 14 6
73-1 8 35 11 8 18 6
22-1 15 30 0 6 25 7
64-3 8 20 11 6 15 10
64-2 0 32 16 8 13 11
64-1 14 12 14 11 18 10
56-1 8 21 0 7 36 11
56-2 0 35 0 8 20 16
23-3 14 0 14 8 12 12
23-1 15 0 0 10 29 14
23-2 7 0 9 7 20 17
49-1 7 0 0 8 21 9
16-1 0 0 13 6 32 15
51-1 0 0 0 7 35 13


Vehicle vt0 nodes: 
0 33 1 43 42 41 63 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
33-2 15 0 14 9 24 15
33-1 7 33 19 15 27 9
1-1 0 30 7 7 13 13
43-1 8 33 10 15 27 9
42-1 8 0 15 7 33 15
41-1 0 0 15 8 26 12
41-3 7 31 0 13 29 10
41-2 14 0 0 9 31 14
63-3 0 31 0 7 24 7
63-1 0 0 7 14 30 8
63-2 0 0 0 12 31 7


Vehicle vt0 nodes: 
0 7 46 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
7-1 0 30 17 14 23 11
7-2 0 0 17 13 30 6
7-3 0 34 0 15 22 17
46-2 11 0 0 7 34 17
46-1 0 0 0 11 34 17


Vehicle vt0 nodes: 
0 5 29 45 4 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-2 14 19 8 7 27 16
5-1 15 0 12 7 19 7
29-2 0 51 0 16 6 8
29-1 0 5 9 12 26 15
29-3 0 36 0 14 8 6
45-3 14 16 0 6 35 8
45-1 0 5 0 14 31 9
45-2 15 0 0 8 16 12
4-1 0 0 0 15 5 15


Vehicle vt0 nodes: 
0 68 2 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
68-2 8 0 14 11 22 7
68-3 0 0 20 6 18 10
68-1 0 29 0 6 27 18
2-3 0 0 12 8 29 8
2-1 8 0 0 12 30 14
2-2 0 0 0 8 28 12


Vehicle vt0 nodes: 
0 48 47 36 37 15 57 13 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
48-2 6 48 16 15 11 9
48-1 6 52 0 19 8 16
47-1 0 33 16 18 13 13
47-2 0 30 0 6 30 16
36-1 8 14 16 15 19 9
37-3 0 12 20 6 14 7
37-1 16 18 0 8 34 16
37-2 0 0 20 10 12 8
15-1 9 14 0 7 34 16
15-2 16 0 14 7 13 15
57-2 16 0 7 7 18 7
57-1 0 0 8 8 23 12
13-3 9 0 7 7 14 10
13-2 9 0 0 15 14 7
13-1 0 0 0 9 30 8


Vehicle vt0 nodes: 
0 34 52 54 27 0 
Packing_pattern: WALL
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
34-1 12 35 7 11 24 9
52-2 12 35 0 10 25 7
52-1 0 35 15 12 15 11
54-1 0 35 0 12 18 15
54-2 19 0 0 5 35 14
54-3 9 0 16 12 34 13
27-2 9 0 0 10 32 16
27-1 0 0 0 9 35 17


Vehicle vt0 nodes: 
0 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 8 19 59 14 53 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
8-1 10 26 17 13 30 10
8-2 10 53 0 13 5 17
19-1 7 0 17 10 26 11
19-2 0 34 7 10 20 13
59-3 0 34 0 8 24 7
59-1 11 24 9 12 29 8
59-2 18 0 0 6 20 17
14-1 0 0 17 7 34 13
53-3 11 24 0 12 22 9
53-2 11 0 0 7 24 17
53-1 0 0 0 11 31 17


Vehicle vt0 nodes: 
0 66 65 10 72 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
66-3 14 35 7 9 19 16
66-2 0 35 16 12 13 12
66-1 14 16 7 11 19 18
65-2 0 19 16 10 16 13
65-1 14 16 0 11 34 7
65-3 0 19 10 12 26 6
10-3 13 0 0 10 16 16
10-1 0 23 0 14 30 10
10-2 0 0 7 13 19 15
72-1 0 0 0 12 23 7



Rounds: 3 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 694437389
Hard_constraints: 0 0
Time: 3238.58
Cost: 1128131
