Total_Vehicles 
6

Vehicle vt0 nodes: 
0 8 1 2 5 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
8-2 0 25 16 12 26 8
8-1 0 26 6 14 23 10
1-1 0 0 11 9 13 11
2-1 0 26 0 15 21 6
2-2 10 0 16 15 25 14
5-1 10 0 0 13 26 16
5-2 0 0 0 10 17 11


Vehicle vt0 nodes: 
0 11 4 3 6 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-3 19 29 0 5 26 6
11-1 8 0 11 14 29 10
11-2 0 45 0 5 14 16
4-1 13 0 0 6 30 11
3-1 0 0 11 8 34 7
3-2 0 32 0 19 13 7
6-1 0 0 0 13 32 11


Vehicle vt0 nodes: 
0 16 21 17 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
16-1 16 0 0 9 29 7
16-2 8 0 14 8 14 8
21-1 8 0 0 7 17 14
17-2 0 0 14 8 26 10
17-1 0 0 0 6 33 14


Vehicle vt0 nodes: 
0 10 7 9 12 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-1 16 0 0 8 34 10
7-3 0 45 11 19 9 13
7-2 0 45 0 23 9 11
7-1 7 14 11 6 29 14
9-2 10 14 0 6 31 11
9-1 0 14 11 7 31 6
12-1 0 14 0 10 25 11
12-2 0 0 0 15 14 12


Vehicle vt0 nodes: 
0 15 18 20 14 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
15-3 15 29 0 10 17 10
15-2 7 24 15 6 26 9
15-1 0 24 15 7 33 15
18-1 0 29 0 15 30 15
20-1 9 0 7 11 24 18
20-2 0 0 20 5 19 7
14-1 9 0 0 11 29 7
14-3 0 0 13 9 19 7
14-2 0 0 0 9 17 13


Vehicle vt0 nodes: 
0 13 19 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-1 11 15 0 11 34 11
19-2 6 0 0 14 15 8
19-3 0 28 0 11 30 11
19-1 0 0 0 6 28 11



Rounds: 2 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 2065496297
Hard_constraints: 0 0
Time: 145.639
Cost: 498330
