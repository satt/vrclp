Total_Vehicles 
8

Vehicle vt0 nodes: 
0 15 16 13 7 10 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
15-2 13 21 10 12 20 12
15-1 13 21 0 11 33 10
16-2 18 0 0 7 21 7
16-1 0 21 16 13 34 14
13-1 0 27 0 13 27 16
13-2 9 0 12 9 17 15
7-1 11 0 0 7 21 12
10-2 0 0 7 9 17 16
10-1 0 0 0 11 27 7


Vehicle vt0 nodes: 
0 18 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
18-1 0 13 0 14 34 16
18-2 0 0 0 16 13 16


Vehicle vt0 nodes: 
0 21 14 17 9 23 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-1 13 0 8 7 24 13
14-1 0 50 0 14 9 16
17-2 14 25 0 9 30 9
17-3 0 12 9 13 35 15
17-1 12 0 0 13 25 8
9-1 0 40 0 14 10 7
9-2 0 33 0 13 7 9
23-2 0 12 0 12 21 9
23-1 0 0 0 10 12 14


Vehicle vt0 nodes: 
0 11 12 8 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-1 14 0 21 10 34 9
11-2 0 52 18 18 5 11
11-3 14 30 0 8 25 14
12-2 0 29 18 14 23 11
12-3 15 0 7 9 29 14
12-1 0 24 0 14 34 18
8-2 15 0 0 10 30 7
8-1 7 0 0 8 24 14
8-3 0 0 0 7 19 11


Vehicle vt0 nodes: 
0 22 2 20 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
22-2 13 0 18 12 34 6
22-3 7 27 0 6 27 14
22-1 15 0 0 10 35 18
2-2 7 0 0 8 27 17
2-1 0 0 13 7 35 17
20-1 0 0 0 7 28 13


Vehicle vt0 nodes: 
0 29 24 1 6 5 4 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
29-2 11 26 21 9 30 9
29-3 11 26 11 7 29 10
29-1 11 32 0 12 26 11
24-1 15 0 21 8 20 9
24-2 0 31 16 11 19 14
1-1 0 32 0 11 28 16
6-3 10 0 21 5 23 9
6-2 0 0 17 10 26 8
6-1 0 0 7 10 31 10
5-2 10 0 10 15 24 11
5-1 10 0 0 14 32 10
4-1 0 0 0 10 26 7


Vehicle vt0 nodes: 
0 26 28 27 25 3 19 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
26-1 5 18 15 9 34 15
28-2 0 53 0 21 6 10
28-1 0 18 15 5 28 13
27-2 0 46 0 21 7 10
27-1 0 18 8 14 28 7
27-3 0 36 0 16 10 8
25-1 14 0 13 9 36 17
3-1 14 0 0 10 31 13
19-3 0 0 22 9 18 8
19-2 0 0 8 14 14 14
19-1 0 0 0 11 35 8


Vehicle vt0 nodes: 
0 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)



Rounds: 3 Idle rounds: 1
Seed: 320424319
Hard_constraints: 0 0
Time: 392.811
Cost: 790160
