Total_Vehicles 
11

Vehicle vt0 nodes: 
0 38 42 43 44 8 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
38-2 17 22 13 8 14 16
38-3 17 0 21 6 15 6
38-1 0 18 22 11 35 7
42-3 12 40 0 9 17 11
42-2 17 22 0 8 18 13
42-1 0 26 12 12 31 10
43-1 0 26 0 12 34 12
43-2 17 0 8 8 22 13
44-1 17 0 0 7 19 8
8-3 12 0 0 5 34 8
8-2 0 0 18 12 18 7
8-1 0 0 0 11 26 18


Vehicle vt0 nodes: 
0 9 37 34 32 33 31 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
9-2 17 38 18 8 13 6
9-1 0 48 14 14 8 14
37-1 0 48 0 14 12 14
34-2 17 38 9 7 13 9
34-3 17 18 9 8 20 13
34-1 17 18 0 7 30 9
32-1 17 0 7 6 18 10
33-1 17 0 0 8 18 7
33-3 0 34 0 15 14 12
33-2 7 0 10 10 34 17
31-2 0 0 10 7 31 9
31-1 0 0 0 15 32 10


Vehicle vt0 nodes: 
0 35 27 29 28 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
35-2 0 8 22 14 32 8
35-3 12 23 11 13 12 8
35-1 0 48 13 17 10 16
27-3 0 8 12 19 15 10
27-1 0 26 13 10 22 9
27-2 0 52 0 16 8 13
29-1 0 26 0 12 25 13
29-3 0 0 12 18 8 11
29-2 12 17 0 12 35 11
28-1 12 0 0 11 17 12
28-2 0 0 0 12 26 12


Vehicle vt0 nodes: 
0 15 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
15-3 0 36 0 15 11 7
15-1 7 0 0 9 29 16
15-2 0 0 0 7 36 12


Vehicle vt0 nodes: 
0 36 7 6 5 4 3 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
36-2 13 36 0 10 21 11
36-1 6 18 21 7 36 8
7-2 0 36 14 13 15 7
7-1 13 22 0 9 14 18
7-3 0 18 21 6 16 9
6-1 0 18 14 13 18 7
5-1 0 22 0 13 28 14
5-2 6 5 13 13 13 15
5-3 0 0 10 6 18 15
4-2 7 0 13 15 5 9
4-1 7 0 0 14 22 13
3-1 0 0 0 7 21 10


Vehicle vt0 nodes: 
0 21 20 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
21-2 10 12 8 5 36 17
21-1 10 12 0 12 34 8
20-2 0 12 0 10 27 16
20-1 0 0 0 21 12 17


Vehicle vt0 nodes: 
0 39 40 41 30 24 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
39-1 13 44 0 12 13 13
39-2 13 0 21 10 32 6
39-3 13 23 13 5 19 8
40-2 0 34 11 11 17 8
40-1 13 0 11 12 23 10
41-2 13 27 0 9 17 13
41-1 0 34 0 13 24 11
41-3 0 0 11 13 34 15
30-1 12 0 0 12 27 11
24-1 0 0 0 12 34 11


Vehicle vt0 nodes: 
0 19 26 22 23 25 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
19-2 6 27 14 6 31 13
19-1 0 20 14 6 33 14
19-3 14 27 10 11 17 9
26-1 14 27 0 11 13 10
26-2 11 0 14 14 27 6
22-1 0 27 0 14 29 14
23-2 0 0 9 11 17 12
23-1 12 0 0 13 27 14
25-1 0 0 0 12 20 9


Vehicle vt0 nodes: 
0 2 14 12 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
2-1 12 24 17 11 28 12
14-3 14 0 0 10 16 16
14-1 12 24 0 13 32 17
14-2 0 31 11 12 23 16
12-1 0 31 0 12 18 11
12-3 0 0 15 10 31 9
12-2 0 0 0 14 24 15


Vehicle vt0 nodes: 
0 16 18 10 0 
Packing_pattern: WALL
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
16-1 0 47 0 17 12 15
18-2 9 12 16 15 25 8
18-1 9 12 0 14 35 16
10-1 0 12 0 9 16 17
10-2 0 0 0 25 12 16


Vehicle vt0 nodes: 
0 17 11 13 1 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 0 46 10 23 10 13
17-3 0 38 17 18 7 13
17-2 0 46 0 25 11 10
11-3 14 6 16 9 32 8
11-2 0 39 0 16 7 17
11-1 14 6 0 9 33 16
13-1 0 6 8 14 29 16
1-1 0 6 0 14 33 8
1-2 0 0 0 20 6 17



Rounds: 4 Idle rounds: 1
Kick Rounds: 2 Kick improving rounds: 1
Seed: 1568466455
Hard_constraints: 0 0
Time: 1304.9
Cost: 1223522
