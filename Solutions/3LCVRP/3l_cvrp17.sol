Total_Vehicles 
14

Vehicle vt0 nodes: 
0 27 13 8 7 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
27-1 0 36 0 11 18 11
27-2 15 0 13 7 14 12
13-1 15 0 0 10 17 13
8-1 6 0 0 9 36 12
7-1 0 0 0 6 35 7


Vehicle vt0 nodes: 
0 1 22 28 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
1-1 0 44 10 23 11 11
1-2 15 0 0 5 32 15
22-2 0 44 0 23 12 10
22-1 0 16 0 15 28 16
28-1 0 0 0 7 16 9


Vehicle vt0 nodes: 
0 34 30 6 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
34-1 18 20 0 7 35 13
34-2 8 0 7 11 20 11
30-1 0 36 0 9 14 8
6-1 8 0 0 10 33 7
6-2 0 0 13 6 36 16
6-3 0 0 0 8 29 13


Vehicle vt0 nodes: 
0 15 20 21 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
15-1 12 8 9 13 17 17
15-2 0 0 21 12 34 7
20-2 5 43 0 16 15 15
20-3 0 0 14 11 33 7
20-1 11 8 0 14 35 9
21-2 0 38 0 5 21 16
21-3 0 8 0 11 30 14
21-1 0 0 0 24 8 10


Vehicle vt0 nodes: 
0 33 16 3 0 
Packing_pattern: WALL
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
33-2 10 30 0 12 24 16
33-1 0 30 0 10 13 7
16-3 14 0 0 7 17 10
16-2 8 0 11 6 28 17
16-1 0 0 11 8 29 17
3-1 0 0 0 14 30 11


Vehicle vt0 nodes: 
0 11 26 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-1 5 0 0 6 31 13
26-1 0 0 0 5 25 13


Vehicle vt0 nodes: 
0 2 4 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
2-1 0 36 11 18 14 18
2-3 5 0 17 13 26 8
2-2 0 36 0 18 14 11
4-3 5 6 0 13 30 17
4-1 0 6 0 5 26 16
4-2 0 0 0 24 6 8


Vehicle vt0 nodes: 
0 17 40 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
17-1 12 7 0 13 27 11
17-3 0 34 0 12 12 11
17-2 0 0 9 6 29 16
40-1 0 7 0 12 27 9
40-2 0 0 0 16 7 8


Vehicle vt0 nodes: 
0 12 39 31 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
12-2 5 0 10 8 20 18
12-1 12 20 0 13 36 7
39-1 0 20 0 12 24 14
39-2 5 0 0 9 20 10
31-1 0 0 0 5 19 13


Vehicle vt0 nodes: 
0 9 32 0 
Packing_pattern: WALL
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
9-2 7 32 0 7 20 9
9-3 0 22 0 7 31 6
9-1 14 0 14 10 32 15
32-3 9 0 14 5 26 16
32-1 9 0 0 15 31 14
32-2 0 0 0 9 22 18


Vehicle vt0 nodes: 
0 10 38 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
10-2 0 10 0 8 27 16
10-1 0 0 6 21 10 17
38-1 0 0 0 19 10 6


Vehicle vt0 nodes: 
0 35 14 19 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
35-2 0 45 9 20 13 14
35-1 18 0 0 6 14 10
14-2 11 0 0 7 28 11
14-1 0 45 0 23 12 9
19-1 0 15 0 6 30 14
19-2 0 0 0 11 15 9


Vehicle vt0 nodes: 
0 25 18 24 23 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
25-2 0 28 7 7 17 16
25-1 19 30 0 5 16 14
18-2 5 6 11 14 19 17
18-1 0 0 9 5 22 11
18-3 8 30 0 11 25 15
24-1 0 30 0 8 29 7
23-3 7 0 11 16 6 11
23-1 7 0 0 13 30 11
23-2 0 0 0 7 28 9


Vehicle vt0 nodes: 
0 29 5 37 36 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
29-1 0 33 0 12 14 17
29-2 12 0 13 7 29 9
5-2 12 17 7 5 35 6
5-1 12 17 0 13 30 7
37-1 12 0 0 10 17 13
37-2 0 0 8 8 28 16
36-1 0 0 0 12 33 8



Rounds: 3 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 679278706
Hard_constraints: 0 0
Time: 531.322
Cost: 864658
