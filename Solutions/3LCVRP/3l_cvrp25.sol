Total_Vehicles 
22

Vehicle vt0 nodes: 
0 58 87 42 37 100 16 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
58-1 15 13 24 9 33 6
87-1 0 13 22 10 18 7
42-1 0 43 0 14 14 15
42-2 0 30 0 12 13 14
37-1 15 13 18 9 33 6
100-1 15 18 0 10 34 18
100-2 17 0 0 5 18 15
100-3 0 0 22 17 13 6
16-2 0 0 13 15 29 9
16-1 0 0 0 13 30 13


Vehicle vt0 nodes: 
0 25 55 54 12 0 
Packing_pattern: LOWBACKLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
25-2 0 0 21 13 36 9
25-1 15 28 0 10 32 12
55-1 0 43 9 15 16 14
55-2 17 0 0 6 28 17
54-2 0 43 0 13 17 9
54-1 0 7 11 15 29 10
12-1 0 7 0 15 36 11
12-2 0 0 0 17 7 16


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 97 14 38 86 44 91 85 98 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
97-1 11 45 9 13 12 17
14-3 11 0 24 5 23 6
14-2 19 0 13 6 22 11
14-1 11 45 0 13 13 9
38-1 11 26 12 12 19 14
86-1 0 22 12 11 34 14
44-1 11 26 0 14 19 12
44-2 19 0 0 6 23 13
91-1 0 26 0 11 32 12
91-3 11 0 16 8 22 8
91-2 11 0 0 8 26 16
85-1 0 0 12 11 22 7
98-1 0 0 0 10 26 12


Vehicle vt0 nodes: 
0 28 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
28-3 0 0 17 23 12 13
28-1 0 15 0 14 29 14
28-2 0 0 0 18 15 17


Vehicle vt0 nodes: 
0 32 90 63 19 48 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
32-2 0 36 14 14 16 16
32-1 0 36 0 12 23 14
32-3 6 0 16 7 36 14
90-1 10 0 8 5 36 8
63-1 15 21 0 10 19 6
63-3 15 0 24 7 21 6
63-2 15 0 15 6 21 9
19-1 0 0 16 6 32 14
48-2 15 0 0 6 19 15
48-3 10 0 0 5 32 8
48-1 0 0 0 10 33 16


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 89 60 45 83 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
89-1 11 34 9 7 24 17
60-2 11 34 0 10 25 9
60-1 8 0 17 12 34 13
45-2 5 30 0 6 29 14
45-3 0 29 0 5 22 15
45-1 8 0 0 15 30 17
83-1 0 0 0 8 29 18


Vehicle vt0 nodes: 
0 31 88 62 10 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
31-1 20 36 0 5 17 15
88-1 14 0 15 8 35 15
88-3 14 0 7 7 30 8
88-2 14 36 0 6 21 16
62-1 0 30 0 14 27 18
62-2 0 13 15 14 17 12
62-3 0 0 15 12 13 15
10-2 14 0 0 7 36 7
10-1 0 0 0 14 26 15


Vehicle vt0 nodes: 
0 51 9 71 65 66 70 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
51-1 0 50 18 14 7 8
9-1 13 13 18 6 21 12
71-1 14 13 9 5 36 9
65-2 0 23 18 13 27 12
65-1 0 28 0 13 32 18
65-3 0 0 15 5 23 11
66-2 14 18 0 8 33 9
66-1 0 5 0 14 23 15
66-3 15 0 7 7 13 18
70-1 15 0 0 10 18 7
70-2 0 0 0 15 5 14


Vehicle vt0 nodes: 
0 6 5 84 17 61 93 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
6-3 14 45 0 8 15 13
6-1 12 18 13 12 15 17
6-2 0 27 17 12 31 11
5-1 14 31 0 9 14 8
84-2 12 0 13 11 18 14
84-1 13 0 0 12 31 13
17-1 0 33 0 14 27 17
61-1 6 0 8 6 27 12
61-3 0 0 12 5 12 8
61-2 6 0 0 7 33 8
93-1 0 0 0 6 27 12


Vehicle vt0 nodes: 
0 57 43 15 21 53 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
57-2 15 0 14 7 28 14
57-1 0 0 17 10 20 10
43-3 7 24 12 8 18 15
43-2 10 0 6 12 21 8
43-1 0 24 15 7 35 12
15-1 7 28 0 13 21 12
15-3 0 24 8 7 35 7
15-2 10 0 0 14 28 6
21-1 0 51 0 23 9 7
53-2 0 24 0 7 27 8
53-1 0 0 0 10 24 17


Vehicle vt0 nodes: 
0 11 64 49 7 52 0 
Packing_pattern: BACKLEFTLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
11-1 13 35 16 9 12 7
64-2 9 18 17 15 15 11
64-1 0 32 15 8 24 13
64-3 0 0 17 9 23 8
49-2 13 35 0 12 23 16
49-3 14 18 0 8 17 17
49-1 0 32 0 13 20 15
7-2 14 0 15 9 12 11
7-3 0 0 10 14 32 7
7-1 14 0 0 8 18 15
52-1 0 0 0 14 27 10


Vehicle vt0 nodes: 
0 26 80 24 29 68 76 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
26-1 18 9 17 7 33 8
26-2 0 34 15 8 14 8
80-1 18 27 0 7 31 17
24-2 18 12 0 7 15 17
24-1 9 12 0 9 30 14
29-1 0 9 15 8 25 8
68-1 0 0 10 20 9 15
76-3 0 46 0 18 14 7
76-1 0 12 0 9 34 15
76-2 0 0 0 24 12 10


Vehicle vt0 nodes: 
0 33 34 35 20 30 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
33-3 0 38 9 9 14 9
33-1 13 29 15 8 31 14
33-2 13 29 8 7 27 7
34-1 13 29 0 8 21 8
35-2 0 24 15 8 14 14
35-3 0 24 9 13 14 6
35-1 0 24 0 10 36 9
20-2 19 0 16 6 29 10
20-1 19 0 0 6 29 16
30-2 0 0 14 11 24 12
30-3 13 0 0 6 26 10
30-1 0 0 0 13 19 14


Vehicle vt0 nodes: 
0 72 75 56 23 67 39 4 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
72-1 9 46 17 16 12 12
75-1 19 0 0 5 32 15
56-1 0 33 7 9 15 14
23-3 11 46 0 12 13 17
23-1 11 13 12 8 33 17
23-2 11 18 0 8 28 12
67-2 10 0 7 9 13 12
67-1 0 33 0 11 27 7
67-3 5 0 14 5 23 9
39-1 10 0 0 7 18 7
4-2 0 0 14 5 19 8
4-1 0 0 0 10 33 14


Vehicle vt0 nodes: 
0 27 69 1 50 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
27-1 0 33 8 5 23 14
27-2 6 41 15 9 19 8
69-2 15 0 10 6 14 17
69-1 6 41 0 13 18 15
69-3 9 0 10 6 23 9
1-2 6 33 0 16 8 11
1-1 0 28 0 6 32 8
1-3 9 0 0 14 33 10
50-2 0 0 10 9 19 16
50-1 0 0 0 9 28 10


Vehicle vt0 nodes: 
0 13 94 40 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-2 15 24 0 7 34 18
13-1 0 34 0 15 26 16
13-3 14 0 15 11 13 14
94-3 0 0 15 14 31 15
94-2 14 0 7 10 12 8
94-1 14 0 0 8 24 7
40-1 0 12 0 12 22 15
40-2 0 0 0 14 12 15


Vehicle vt0 nodes: 
0 96 99 59 92 95 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
96-2 11 36 12 8 13 14
96-1 0 24 15 10 31 15
99-1 0 0 16 8 20 11
59-1 11 0 12 8 36 15
59-2 11 31 0 11 20 12
92-1 0 24 0 11 32 15
92-3 0 0 9 10 24 7
92-2 13 0 0 8 31 12
95-1 0 0 0 13 23 9


Vehicle vt0 nodes: 
0 77 3 79 78 81 0 
Packing_pattern: LEFTLOWBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
77-1 8 0 14 10 31 16
3-1 0 32 15 13 28 14
79-1 0 33 0 14 27 15
78-2 8 0 0 13 33 14
78-1 0 0 15 6 32 10
81-1 0 0 0 8 32 15


Vehicle vt0 nodes: 
0 73 74 22 41 2 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
73-1 9 31 11 15 11 7
74-2 9 0 21 9 24 9
74-1 10 0 11 15 31 10
22-3 0 28 8 9 24 18
22-1 10 0 0 14 24 11
22-2 9 42 0 13 14 12
41-2 9 28 0 12 14 11
41-1 0 28 0 9 30 8
2-1 0 0 0 10 28 16


Vehicle vt0 nodes: 
0 18 8 46 36 47 82 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
18-3 12 26 12 6 19 17
18-1 12 43 0 13 15 12
18-2 7 26 12 5 20 7
8-1 19 20 0 6 22 8
46-2 0 25 18 6 22 11
46-1 7 26 0 12 17 12
36-3 14 0 16 8 13 12
36-2 0 26 0 7 31 18
36-1 0 0 17 14 25 13
47-2 0 21 0 16 5 17
47-1 12 0 0 12 20 16
82-1 0 0 0 12 21 17



Rounds: 3 Idle rounds: 1
Kick Rounds: 2 Kick improving rounds: 1
Seed: 212888478
Hard_constraints: 0 0
Time: 5563.65
Cost: 1435909
