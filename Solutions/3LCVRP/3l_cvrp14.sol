Total_Vehicles 
9

Vehicle vt0 nodes: 
0 5 6 32 9 4 0 
Packing_pattern: LEFTBACKLOW
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
5-2 0 30 18 7 26 10
5-1 0 53 0 19 7 15
6-2 7 17 22 15 30 6
6-1 11 0 16 14 17 9
6-3 0 16 18 7 14 12
32-3 0 0 22 11 16 6
32-2 8 18 15 13 32 7
32-1 14 0 0 11 18 16
9-1 11 18 0 14 35 15
9-2 0 0 11 10 14 11
4-3 0 18 10 8 34 8
4-1 0 18 0 11 29 10
4-2 0 0 0 14 18 11


Vehicle vt0 nodes: 
0 2 12 7 8 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
2-2 7 28 16 8 28 12
2-3 0 0 18 7 29 10
2-1 10 0 20 14 21 10
12-1 0 23 7 7 14 11
12-2 20 28 0 5 27 16
7-1 7 28 0 13 32 16
7-2 0 23 0 7 34 7
8-3 10 0 10 12 28 10
8-1 10 0 0 14 28 10
8-2 0 0 0 10 23 18


Vehicle vt0 nodes: 
0 14 15 17 26 27 16 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
14-1 0 25 14 10 34 11
14-2 20 30 0 5 28 10
15-1 19 0 14 6 28 15
17-2 14 0 14 5 36 9
17-1 0 0 18 12 21 8
26-2 14 30 0 6 28 13
26-1 14 0 0 11 30 14
26-3 9 25 0 5 17 10
27-1 0 25 0 9 35 14
16-1 0 0 0 14 25 18


Vehicle vt0 nodes: 
0 30 25 28 29 0 
Packing_pattern: AREANOWALLS
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
30-1 15 12 8 6 23 18
25-1 0 45 0 20 13 14
25-2 15 12 0 10 29 8
28-2 0 12 6 15 33 17
28-1 0 0 15 11 12 11
29-2 0 12 0 14 29 6
29-1 0 0 0 23 12 15


Vehicle vt0 nodes: 
0 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 3 11 1 31 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
3-2 12 24 16 13 14 12
3-1 13 32 0 11 26 16
3-3 13 0 16 9 24 7
11-2 13 18 0 11 14 16
11-1 0 21 9 12 35 16
11-3 0 0 17 12 21 11
1-1 0 20 0 13 30 9
1-2 13 0 0 7 18 16
31-1 0 0 0 13 20 17


Vehicle vt0 nodes: 
0 0 
Packing_pattern: LOWLEFTBACK
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)


Vehicle vt0 nodes: 
0 13 19 22 20 18 0 
Packing_pattern: BACKLOWLEFT
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
13-3 5 36 11 12 18 14
13-1 5 32 0 9 28 11
13-2 0 28 0 5 15 17
19-1 6 0 17 7 32 9
22-2 0 0 19 6 24 7
22-1 0 0 9 6 22 10
20-3 14 21 7 9 15 16
20-1 14 21 0 11 35 7
20-2 14 0 10 11 16 15
18-2 14 0 0 10 21 10
18-1 6 0 0 8 26 17
18-3 0 0 0 6 28 9


Vehicle vt0 nodes: 
0 24 23 21 10 0 
Packing_pattern: AREA
Item ID [placed_w, placed_l, placed_z] (Dimension_w, Dimension_l, Dimension_z)
24-2 0 48 18 17 11 8
24-3 0 21 20 13 27 7
24-1 0 50 0 21 10 18
23-1 13 25 0 8 16 18
23-2 0 37 11 12 11 9
23-3 13 0 7 9 20 17
21-2 0 21 11 13 16 9
21-3 13 0 0 9 25 7
21-1 0 21 0 13 29 11
10-1 0 0 8 13 21 17
10-2 0 0 0 12 21 8



Rounds: 4 Idle rounds: 1
Kick Rounds: 1 Kick improving rounds: 0
Seed: 115291390
Hard_constraints: 0 0
Time: 658.385
Cost: 1402312
